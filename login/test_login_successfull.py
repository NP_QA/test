import logging
import pytest
import allure
from Login_Page import SearchHelper


@pytest.mark.smoke_test_cv
@allure.feature('Smoke тестирование на Chrome. Авторизация.')
@allure.story('Успешная авторизация')
@allure.severity('Critical')
def test_login_successfull_chrome(test_chrome):
    try:
        logger = logging.getLogger('CV_logger')
        logger.setLevel(logging.DEBUG)
        # create file handler which logs even debug messages
        fh = logging.FileHandler('admin_login_successfull.txt')
        fh.setLevel(logging.DEBUG)
        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)
        consoleHandler = logging.StreamHandler()
        consoleHandler.setLevel(logging.DEBUG)
        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
        ch.setFormatter(formatter)
        fh.setFormatter(formatter)
        # add the handlers to logger
        logger.addHandler(ch)
        logger.addHandler(fh)
        cv_login = SearchHelper(test_chrome)
        cv_login.go_to_login()
        logging.debug('Перешел на страницу логина')
        # Логин и пароль изменять в зависимости от прав данного пользователя на сайте
        cv_login.login_input('tuser')
        cv_login.login_password('951753')
        cv_login.click_login_button()
        cv_login.assert_login_successfull()
    except:
        logging.exception('')
        logging.critical('TEST FAILED')
        exit(1)
