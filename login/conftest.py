import pytest
from selenium import webdriver

@pytest.fixture(scope="session")
def test_chrome():
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--disable-gpu')
    driver = webdriver.Remote(command_executor="http://selenium__standalone-firefox:4444/wd/hub",
    desired_capabilities={'browserName': 'firefox'})
    yield driver
    driver.quit()

