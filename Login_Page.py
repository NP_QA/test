import logging
import time
import allure
from allure_commons.types import AttachmentType
from selenium.webdriver.common.by import By
from BaseApp import BasePage


class PageLocators:
    LOCATOR_CV_GO_TO_LOGIN = 'https://ui-demo.water.rusatom.dev/login'
    LOCATOR_CV_LOGIN_INPUT = (By.XPATH, '//div[2]//input[@class="form-control water-input-field"]')
    LOCATOR_CV_LOGIN_PASSWORD_INPUT = (By.XPATH, '//div[3]/div/div/input[@class="form-control water-input-field"]')
    LOCATOR_CV_LOGIN_BUTTON = (By.XPATH, '//button[@class="btn btn-rocky btn-block"]')
    LOCATOR_CV_ASSERT_LOGIN_SUCCESSFULL = (By.XPATH, '//*[contains(text(), "Начните вводить название системы")]')
    LOCATOR_CV_ASSERT_WRONG_LOGIN = (By.XPATH, '//*[@id="toast-container"]/div/div[2]')
    LOCATOR_CV_ASSERT_WRONG_PASSWORD = (By.XPATH, '//*[@id="toast-container"]/div/div[2]')
    LOCATOR_CV_ASSERT_NO_ACCESS_RIGHT = (By.XPATH, '//*[@id="toast-container"]/div/div[2]')


class SearchHelper(BasePage):
    def go_to_login(self):
        try:
            if self.driver.get(PageLocators.LOCATOR_CV_GO_TO_LOGIN):
                with allure.step('Переход на страницу логина. Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Переход на страницу логина. Успешный.',
                                  attachment_type=AttachmentType.PNG)
                        
        except:
            with allure.step('Переход на страницу логина. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Переход на страницу логина. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def login_input(self, login):
        try:
            if self.find_element(PageLocators.LOCATOR_CV_LOGIN_INPUT).send_keys(login):
                with allure.step('Заполнение логина. Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Переход на страницу логина. Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Заполнение логина. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Переход на страницу логина. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def login_password(self, password):
        try:
            if self.find_element(PageLocators.LOCATOR_CV_LOGIN_PASSWORD_INPUT).send_keys(password):
                with allure.step('Заполнение пароля. Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Переход на страницу логина. Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Заполнение пароля. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Переход на страницу логина. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def click_login_button(self):
        try:
            if self.find_clickable_element(PageLocators.LOCATOR_CV_LOGIN_BUTTON):
                with allure.step('Клик на кнопку логина. Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Клик на кнопку логина. Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Клик на кнопку логина. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Клик на кнопку логина. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)

    def assert_login_successfull(self):
        try:
            if self.find_element(PageLocators.LOCATOR_CV_ASSERT_LOGIN_SUCCESSFULL):
                with allure.step('Убедился в логине, найдя поисковую строку. Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(), name='Убедился в логине, найдя поисковую '
                                                                            'строку. Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Убедился в логине, найдя поисковую строку. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(), name='Убедился в логине, найдя поисковую строку.')

    def assert_wrong_login(self):
        try:
            time.sleep(2.5)
            if (self.find_element(PageLocators.LOCATOR_CV_ASSERT_WRONG_LOGIN) and (
                    self.find_element(PageLocators.LOCATOR_CV_LOGIN_BUTTON))):
                with allure.step('Убедился, что появилось уведомление о неверном логине. Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(),
                                  name='Убедился, что появилось уведомление о неверном логине. Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Убедился, что появилось уведомление о неверном логине. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Убедился, что появилось уведомление о неверном логине. Провален.',
                              attachment_type=AttachmentType.PNG)

    def assert_wrong_password(self):
        try:
            time.sleep(2.5)
            if (self.find_element(PageLocators.LOCATOR_CV_ASSERT_WRONG_LOGIN) and (
                    self.find_element(PageLocators.LOCATOR_CV_LOGIN_BUTTON))):
                with allure.step('Убедился, что появилось уведомление о неверном пароле логине. Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(),
                                  name='Убедился, что появилось уведомление о неверном пароле логине. Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Убедился, что появилось уведомление о неверном пароле логине. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Убедился, что появилось уведомление о неверном пароле логине. Провален.',
                              attachment_type=AttachmentType.PNG)

    def assert_no_access_right(self):
        try:
            time.sleep(2.5)
            if (self.find_element(PageLocators.LOCATOR_CV_ASSERT_WRONG_LOGIN) and (
                    self.find_element(PageLocators.LOCATOR_CV_LOGIN_BUTTON))):
                with allure.step('Убедился, что появилось уведомление об отсутсвии разрешения на вход. Успешный.'):
                    allure.attach(self.driver.get_screenshot_as_png(),
                                  name='Убедился, что появилось уведомление об отсутсвии разрешения на вход. Успешный.',
                                  attachment_type=AttachmentType.PNG)
        except:
            with allure.step('Убедился, что появилось уведомление об отсутсвии разрешения на вход. Провален.'):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='Убедился, что появилось уведомление об отсутсвии разрешения на вход. Провален.',
                              attachment_type=AttachmentType.PNG)
            logging.exception('')
            logging.critical('TEST FAILED')
            exit(1)
