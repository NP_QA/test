## Подготовка.


1.1 Устанавливается Python (разработано на v.3.8).

1.2 Клонируется репозиторий https://gitlab.com/rosatom/water/test/autotests.git.

1.3 Создается проект и вируальное окружение.

1.4 Устанавливаются библиоткеки из requirements.txt.

1.5 Скачивается драйвер браузера GoogleChrome из https://sites.google.com/a/chromium.org/chromedriver/

(разработано на v.89.0.4389.23) в папку с проектом.

1.6 В файле каждого теста "conftest.py" в строке "driver = webdriver.Chrome(executable_path='С:/Driver_Chrom/chromedriver.exe')"

путь 'С:/Driver_Chrome/chromedriver.exe' заменяется на путь к существующему драйверу.[п.1.5].

1.7 Скачивается архив Allure2 из https://github.com/allure-framework/allure2

1.8 Распаковыввается в выбранную директорию, например "C://Allure".

1.9 Прописать путь паке "bin"  в системных переменных, например "D:\allure\bin"


## Запуск тестов.


2.1 В терминале переход в папку с тестом, например: "cd C:/Login_page/login_wrong_login".

2.2 В терминале вводится команда запуска теста "pytest --alluredir results"

2.2 После завершения тестирования, в терминале вводится команда запуска сбора отчёта "allure serve results"


3. Должен открытья браузер с отображением отчета тестирования.


   




