FROM python:3.8-buster

ENV PYTHONUNBUFFERED 1

ADD . C:\Users\pupme\PycharmProjects\Vodokanal
WORKDIR C:\Users\pupme\PycharmProjects\Vodokanal


RUN pip install -r requirements.txt
RUN pip install pytest
# install google chrome
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'
RUN apt-get -y update
RUN apt-get install -y google-chrome-stable

# install chromedriver
RUN apt-get install -yqq unzip
RUN wget -O /tmp/chromedriver.zip http://chromedriver.storage.googleapis.com/`curl -sS chromedriver.storage.googleapis.com/LATEST_RELEASE`/chromedriver_linux64.zip
RUN unzip /tmp/chromedriver.zip chromedriver -d /usr/local/bin/

# set display port to avoid crash
ENV DISPLAY=:99

RUN mkdir C:\Users\pupme\PycharmProjects\Vodokanal
WORKDIR C:\Users\pupme\PycharmProjects\Vodokanal

COPY . C:\Users\pupme\PycharmProjects\Vodokanal
VOLUME /my_volume

CMD ["pytest"]
